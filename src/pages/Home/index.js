import { useNavigate } from 'react-router'

const Home = () => {
  // get the logged in user's information
  const { id, firstName, lastName } = sessionStorage
  const navigate = useNavigate()

  const logoutUser = () => {
    // remove the logged users details from session storage
    sessionStorage.removeItem('id')
    sessionStorage.removeItem('firstName')
    sessionStorage.removeItem('lastName')

    // navigate to sign in component
    navigate('/signin')
  }

  return (
    <div>
      <h1>Blogs</h1>
      <div className="btn-group" role="group">
        <button
          id="btnGroupDrop1"
          type="button"
          className="btn btn-primary dropdown-toggle"
          data-bs-toggle="dropdown"
          aria-expanded="false"
        >
          Welcome {firstName}
        </button>
        <ul className="dropdown-menu" aria-labelledby="btnGroupDrop1">
          <li>
            <a className="dropdown-item">Profile</a>
          </li>
          <li>
            <button onClick={logoutUser} className="dropdown-item">
              Logout
            </button>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default Home
